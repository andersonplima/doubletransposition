﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DoubleTransposition
{
    class Transposition<T>
    {
        private int[] key;
        private int[] inv_key;
        private int cols;
        private int rows;
        private int size;

        private int[] Invert(int[] key)
        {
            var inv_key = new int[key.Length];
            for (int i = 0; i != key.Length; ++i)
                inv_key[key[i]] = i;
            return inv_key;
        }

        public Transposition(int[] key, int cols, int rows)
        {
            if (key.Length != cols)
                throw new ArgumentException("Invalid key size!", "key");

            this.key = key;
            this.cols = cols;
            this.rows = rows;
            this.size = cols * rows;

            this.inv_key = Invert(key);
        }

        public T[] Forward(T[] d)
        {
            var dt = new T[size];

            for (int p = 0; p < size; ++p)
            {
                var i = p % cols;
                var j = p / cols;
                dt[j + key[i] * rows] = d[p];
            }

            return dt;
        }

        public T[] Backward(T[] dt)
        {
            var d = new T[size];

            for (int p = 0; p < size; ++p)
            {
                var i = p % rows;
                var j = p / rows;
                d[inv_key[j] + i * cols] = dt[p];
            }

            return d;
        }
    }
}
