﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace DoubleTransposition
{
    public class CRC
    {
        private byte[] polinomial;
        private int polinomialBitLength;

        public CRC(byte[] polinomial, int polinomialBitLength)
        {
            this.polinomial = polinomial;
            this.polinomialBitLength = polinomialBitLength;
        }

        private byte[] StuffPolinomial(int length)
        {
            var ret = new byte[length];
            Array.Copy(polinomial, ret, polinomial.Length);

            return ret;
        }

        private void ShiftRight(byte[] arr)
        {
            var last_bit = 0x00;
            for (int j = 0; j < arr.Length; ++j)
            {
                var next_last_bit = arr[j] & 0x01;
                arr[j] = (byte)((arr[j] >> 1) | (last_bit << 7));
                last_bit = next_last_bit;
            }
        }

        public byte[] Compute(byte[] value)
        {
            var value_length = value.Length;
            var stuffed_length = value_length + polinomial.Length;
            var stuffed_polinomial = StuffPolinomial(stuffed_length);

            var intermediate = new byte[stuffed_length];
            Array.Copy(value, intermediate, value_length);

            for (int i = 0, mask = 0x80; i < value_length * 8; ++i, mask = (mask == 0x01) ? 0x80 : (mask >> 1))
            {
                if ((intermediate[i / 8] & mask) != 0)
                    for (int j = 0; j < stuffed_length; ++j)
                        intermediate[j] = (byte)(intermediate[j] ^ stuffed_polinomial[j]);

                ShiftRight(stuffed_polinomial);
            }

            var ret = new byte[polinomial.Length];
            Array.Copy(intermediate, value_length, ret, 0, polinomial.Length);

            for (var i = 0; i < ret.Length * 8 - polinomialBitLength + 1; ++i)
                ShiftRight(ret);

            return ret;
        }
    }
}
