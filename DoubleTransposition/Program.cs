﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace DoubleTransposition
{
    class Program
    {
        static void TestString(string message = "Thequickbrownfoxjumpsoverthelazydog")
        {
            Console.WriteLine($"Original message: {message}");

            var key = new[] { 4, 5, 3, 1, 2, 0 };
            var transposition = new Transposition<char>(key, 6, 5);

            var d = message.ToCharArray();
            var dt = transposition.Forward(d);
            dt = transposition.Forward(dt); //twice

            var t_message = new string(dt);

            Console.WriteLine($"Transformed message: {t_message}");

            var d1 = transposition.Backward(dt);
            d1 = transposition.Backward(d1); //twice

            var o_message = new string(d1);

            Console.WriteLine($"Back to original again: {o_message}");

            Console.ReadLine();
        }

        static void PrettyPrint(string header, byte[] arr)
        {
            Console.WriteLine(header);

            foreach (var b in arr)
                Console.Write("{0:x2} ", b);

            Console.WriteLine();
        }

        static void TestMessage(int[] key, byte[] message)
        {
            var full_message = SendMessage(key, message);

            Console.WriteLine();

            PrettyPrint("Sended package: ", full_message);

            var received_message = ReceiveMessage(full_message);

            Console.WriteLine();

            PrettyPrint("Received (translated) package: ", received_message);

            Console.ReadLine();
        }

        static byte[] SendMessage()
        {
            //Random message for example
            var message = new byte[35];
            using (RNGCryptoServiceProvider rng_message = new RNGCryptoServiceProvider())
                rng_message.GetNonZeroBytes(message);

            //Random key (sequential key ordered by random byte)
            var key = new int[] { 0, 1, 2, 3, 4 };
            using (RNGCryptoServiceProvider rng_key = new RNGCryptoServiceProvider())
                key = key.OrderBy(k =>
                {
                    var arr = new byte[1];
                    rng_key.GetBytes(arr);
                    return arr[0];
                }).ToArray();

            return SendMessage(key, message);
        }

        static byte[] SendMessage(int[] key, byte[] message)
        {
            PrettyPrint("Sending message: ", message);

            //Shuffle message 
            var transposition = new Transposition<byte>(key, 5, 7);
            var shuffle_message = transposition.Forward(message);
            shuffle_message = transposition.Forward(shuffle_message); //again

            //Get key bytes to send
            var key_bytes = new byte[2];
            //Position of column 0 (3 bits) + Position of column 1 (3 bits) + Position of column 2 (2 primeiros bits)
            key_bytes[0] = (byte)((key[0] << 5) | (key[1] << 2) | (key[2] >> 1));
            //Position of column 2 (último bit) + 0 + Position of column 3 (3 bits) + Position of column 4 (3 bits)
            key_bytes[1] = (byte)((key[2] << 7) | (key[3] << 3) | key[4]);

            var message_with_key = new byte[37];
            Array.Copy(shuffle_message, message_with_key, 35);
            Array.Copy(key_bytes, 0, message_with_key, 35, 2);

            //Calculate CRC-8
            var crc = new CRC(new byte[] { 0x98, 0x80 }, 9); // CRC polinomial fixed to 100110001
            var crc_byte = crc.Compute(message_with_key)[1]; // It's the same size of polinomial. When we take off the padding, we get just one byte (8-bit CRC).

            var full_message = new byte[38];
            Array.Copy(message_with_key, full_message, 37);
            full_message[37] = crc_byte;

            return full_message;
        }

        static byte[] ReceiveMessage(byte[] message)
        {
            if (message.Length != 38)
                throw new ArgumentException();

            //Get message part + key
            var main_message = new byte[37];
            Array.Copy(message, main_message, 37);

            //Check CRC
            var received_crc = message[message.Length - 1];
            var computed_crc = new CRC(new byte[] { 0x98, 0x80 }, 9).Compute(main_message)[1];
            if (computed_crc != received_crc)
                throw new InvalidOperationException("Byte de checagem inválido!");

            //Key part
            var key_bytes = new byte[2];
            Array.Copy(message, 35, key_bytes, 0, 2);

            var key = new int[5];
            key[0] = (0xe0 & key_bytes[0]) >> 5;                           //1110 0000 mask
            key[1] = (0x1c & key_bytes[0]) >> 2;                           //0001 1100 mask
            key[2] = ((0x03 & key_bytes[0]) << 1) | ((0x80 & key_bytes[1]) >> 7); //0000 0011 mask + 1000 0000 mask
            key[3] = (0x38 & key_bytes[1]) >> 3;                           //0011 1000 mask
            key[4] = 0x07 & key_bytes[1];                           //0000 0111 mask

            //Trash two last bytes
            Array.Resize(ref main_message, 35);

            //Unshuffle message 
            var transposition = new Transposition<byte>(key, 5, 7);
            var original_message = transposition.Backward(main_message);
            original_message = transposition.Backward(original_message); //again

            return original_message;
        }

        static void TestCRC()
        {
            Console.WriteLine("CRC TEST");

            var polinomial = new byte[] { 0x98, 0x80 }; //1001 1000 1000 0000
            Console.WriteLine("Polinomial: {0}|{1}",
                Convert.ToString(polinomial[0], 2),
                Convert.ToString(polinomial[1], 2));

            var crc = new CRC(polinomial, 9);
            var value = new byte[] { 0xe1, 0x00, 0xca, 0xfe };

            Console.WriteLine("Value: {0}|{1}|{2}|{3}",
                Convert.ToString(value[0], 2),
                Convert.ToString(value[1], 2),
                Convert.ToString(value[2], 2),
                Convert.ToString(value[3], 2));

            var crc_value = crc.Compute(value);
            Console.WriteLine("CRC: {0}|{1}",
                Convert.ToString(crc_value[0], 2),
                Convert.ToString(crc_value[1], 2));

            Console.ReadLine();
        }

        static void TestCRC2()
        {
            Console.WriteLine("CRC TEST 2");

            var polinomial = new byte[] { 0xb0 }; // 1011 0000
            Console.WriteLine("Polinomial: {0}", Convert.ToString(polinomial[0], 2));

            var crc = new CRC(polinomial, 4);
            var value = new byte[] { 0x34, 0xec }; // 0011 0100 1110 1100 

            Console.WriteLine("Value: {0}|{1}",
                Convert.ToString(value[0], 2),
                Convert.ToString(value[1], 2));

            var crc_value = crc.Compute(value);
            Console.WriteLine("CRC: {0}",
                Convert.ToString(crc_value[0], 2));

            Console.ReadLine();
        }

        static void Main(string[] args)
        {
            //TestString("A0805509C400000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF");

            //TestCRC();

            //TestCRC2();

            TestMessage(new int[] { 4, 3, 2, 1, 0 },
                new Regex(".{2}")
                    .Matches("A0805509C400000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
                    .Cast<Match>().Select(m => Convert.ToByte("0x" + m.Value, 16)).ToArray());
        }
    }
}
